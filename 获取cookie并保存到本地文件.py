
import urllib2      
import cookielib
##申明一个cookieJar对象实例来保存cookie
#cookie=cookielib.CookieJar()
##利用urllib2库的HTTPCookieProcess对象来创建cookie处理器
#handler=urllib2.HTTPCookieProcessor(cookie)
##通过handle来构建opener
#opener=urllib2.build_opener(handler)
filename='cookie.txt'
cookie=cookielib.MozillaCookieJar(filename)
handler=urllib2.HTTPCookieProcessor(cookie)

opner=urllib2.build_opener(handler)

#此处open方法同URLlib的urlopen方法，也可以传入request
response=opner.open("http://www.baidu.com")
for item in cookie:
    print 'Name='+item.name
    print 'Value='+item.value
cookie.save(ignore_discard=True,ignore_expires=True)