class Alarmsensor(object):
    def run(self):
        print "Alarm Ring..."

class WaterSprinker(object):
    def run(self):
        print "Spray Water..."

class EmergencyDialer(object):
    def run(self):
        print "Dial 119"

#
# if __name__=='__main__':
#     alarm_sensor=Alarmsensor()
#     water_sprinker=WaterSprinker()
#     emergency_dialer=EmergencyDialer()
#
#     alarm_sensor.run()
#     water_sprinker.run()
#     emergency_dialer.run()


class EmergencyFacade(object):
    def __init__(self):
        self.alarm_sensor=Alarmsensor()
        self.water_sprinker=WaterSprinker()
        self.emergency_dialer=EmergencyDialer()

    def runall(self):
        self.alarm_sensor.run()
        self.water_sprinker.run()
        self.emergency_dialer.run()


if __name__=="__main__":
    emegency_facade=EmergencyFacade()
    emegency_facade.runall()