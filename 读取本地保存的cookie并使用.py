import cookielib
import urllib2

#创建MozillzCookieJar实力对象
cookie=cookielib.MozillaCookieJar()
#从文件中读取cookie内容到变量
cookie.load('cookie.txt',ignore_discard=True,ignore_expires=True)

req=urllib2.Request("http://www.baidu.com")

#利用urlib2的build_opener方法创建一个opener
opener=urllib2.build_opener(urllib2.HTTPCookieProcessor(cookie))

response=opener.open(req)

print response.read()